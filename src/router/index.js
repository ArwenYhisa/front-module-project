import Vue from 'vue'
import Router from 'vue-router'
import GraphiquePage from '../components/GraphiquePage'
import TablePage from '../components/TablePage'

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'TablePage',
      component: TablePage,
      props: true
    },
    {
      path: '/charts',
      name: 'GraphiquePage',
      component: GraphiquePage,
      props: true
    }
  ]
})
