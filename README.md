# front-project

> Projet de Front en VueJs de Zoë BONNICI

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

```

Cahier des charges:
- Consulter un tableau paginé des données -> Fait avec BootstrapVue
- Visualiser les données sous forme de graphiques (camembert & histogramme) -> Fait avec ChartJs et l'update est débugger grâce à vous ^^
- Effectuer une recherche dans le tableau de données -> Utilisation de propriété 'filter' de b-table
- Filtrer les données en utilisant un formulaire simple -> Un formulaire permet d'ajouter des filtres et de les reset
- Consulter le détail de chaque ligne du tableau, et modifier les données -> En utilisant la propriété @row-clicked' de b-table, je créer un formulaire contenant toutes les données de la personnes sélectionnée. Ces données sont modifiable.
- Exporter les données modifiées sous forme de JSON et CSV -> Avec vue-blob-json-csv pour le Json et vue-json-csv pour le CSV après avoir mis l'objet "à plat"

